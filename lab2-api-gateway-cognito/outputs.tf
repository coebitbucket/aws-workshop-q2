output "api_url" {
  value = module.apigw.api_url
}

output "api_methods" {
  value = module.apigw.api_method
}

output "api_path" {
  value = module.apigw.api_path
}

output "cognito_pool_id" {
  value = module.cognitoauth.cognito_client_id
}

output "cognito_pool_arn" {
  value = module.cognitoauth.cognito_user_pool_arn
}
output "cognito_client_id" {
  value = module.cognitoauth.cognito_client_id
}

output "cognito_client_secret" {
  value = module.cognitoauth.cognito_client_secret
}