terraform {
  backend "s3" {
    key     = "lab2-terraform-api/api-gw"
    region = var.region
  }
}
