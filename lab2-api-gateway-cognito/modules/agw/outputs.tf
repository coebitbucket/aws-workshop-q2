output "api_name" {
  value = aws_api_gateway_rest_api.api.name
}

output "api_url" {
  value = aws_api_gateway_deployment.api.invoke_url
}

output "api_method" {
  value = aws_api_gateway_method.proxy.http_method
}

output "api_path" {
  value = aws_api_gateway_resource.proxy.path_part
}

output "authorizer_id" {
  value = aws_api_gateway_authorizer.authorizer.id
}