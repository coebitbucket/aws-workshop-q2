resource "aws_api_gateway_rest_api" "api" {
  name        = var.name
  description = var.description
}

resource "aws_api_gateway_model" "gwmodel" {
  rest_api_id  = aws_api_gateway_rest_api.api.id
  name         = var.model_name
  description  = "JSON schema of request"
  content_type = "application/json"
  schema       = <<EOF
{
  "$schema" : "http://json-schema.org/draft-04/schema#",
  "title" : "Request Schema",
  "type" : "object",
  "properties" : {
    "city" : { "type" : "string" }
  }
}
EOF
}

resource "aws_api_gateway_resource" "proxy" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = var.path_part
}

resource "aws_api_gateway_method" "proxy" {
  rest_api_id    = aws_api_gateway_rest_api.api.id
  resource_id    = aws_api_gateway_resource.proxy.id
  http_method    = var.http_method
  authorization  = var.authorization_type
  authorizer_id  = var.authorization_provider
  request_models = { "application/json" = aws_api_gateway_model.gwmodel.name }
}

resource "aws_api_gateway_method_response" "proxy_response_200" {
  rest_api_id     = aws_api_gateway_rest_api.api.id
  resource_id     = aws_api_gateway_resource.proxy.id
  http_method     = aws_api_gateway_method.proxy.http_method
  status_code     = var.status_code
  response_models = { "application/json" = aws_api_gateway_model.gwmodel.name }
}

resource "aws_api_gateway_integration" "proxy_integration" {
  rest_api_id             = aws_api_gateway_rest_api.api.id
  resource_id             = aws_api_gateway_resource.proxy.id
  http_method             = aws_api_gateway_method.proxy.http_method
  type                    = var.integration_type
  integration_http_method = var.http_method
  uri                     = var.lambda_invoke_arn
}

resource "aws_api_gateway_integration_response" "proxy_integration_response" {
  rest_api_id        = aws_api_gateway_rest_api.api.id
  resource_id        = aws_api_gateway_resource.proxy.id
  http_method        = aws_api_gateway_method.proxy.http_method
  status_code        = aws_api_gateway_method_response.proxy_response_200.status_code
  response_templates = { "application/json" = "" }
  depends_on         = [aws_api_gateway_integration.proxy_integration]

}

resource "aws_api_gateway_method" "proxy_root" {
  rest_api_id    = aws_api_gateway_rest_api.api.id
  resource_id    = aws_api_gateway_rest_api.api.root_resource_id
  http_method    = var.http_method
  authorization  = var.authorization
  request_models = { "application/json" : aws_api_gateway_model.gwmodel.name }

}

resource "aws_api_gateway_integration" "proxy_root_integration" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_method.proxy_root.resource_id
  http_method = aws_api_gateway_method.proxy_root.http_method

  integration_http_method = var.http_method
  type                    = "AWS_PROXY"
  uri                     = var.lambda_invoke_arn
}

resource "aws_api_gateway_deployment" "api" {
  depends_on = [
    aws_api_gateway_integration.proxy_root_integration,
    aws_api_gateway_integration.proxy_integration,
  ]

  rest_api_id = aws_api_gateway_rest_api.api.id
  stage_name  = ""
}

resource "aws_cloudwatch_log_group" "api-logs" {
  name              = "API-Gateway-Execution-Logs_${aws_api_gateway_rest_api.api.id}/${var.stage_name}"
  retention_in_days = 3
}

resource "aws_api_gateway_stage" "api-logs" {
  stage_name = var.stage_name
  rest_api_id = aws_api_gateway_rest_api.api.id
  deployment_id = aws_api_gateway_deployment.api.id

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.api-logs.arn
    format          = "$context.identity.sourceIp $context.identity.caller $context.identity.user [$context.requestTime] $context.httpMethod $context.resourcePath $context.protocol $context.status $context.responseLength $context.requestId"

  }
}

resource "aws_lambda_permission" "agwaccess" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = var.lambda_function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.api.execution_arn}/*/*"
}

resource "aws_api_gateway_authorizer" "authorizer" {
  name            = var.authorizer_name
  rest_api_id     = aws_api_gateway_rest_api.api.id
  type            = var.auth_type
  provider_arns   = [var.provider_arn]
  identity_source = "method.request.header.X-COG-TOKEN"
}

resource "aws_api_gateway_account" "logging" {
  cloudwatch_role_arn = aws_iam_role.cloudwatch.arn
}

resource "aws_iam_role" "cloudwatch" {
  name = "api_gateway_cloudwatch_global_${var.stage_name}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "cloudwatch" {
  name = "cloudwatch-logs-${var.stage_name}"
  role = aws_iam_role.cloudwatch.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:DescribeLogGroups",
                "logs:DescribeLogStreams",
                "logs:PutLogEvents",
                "logs:GetLogEvents",
                "logs:FilterLogEvents"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}
