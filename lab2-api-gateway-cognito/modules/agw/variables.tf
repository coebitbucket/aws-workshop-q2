variable "name" {
  description = "API endpoint name"
}

variable "description" {
  description = "API Gateway description"
}

variable "model_name" {
  description = "Gateway model name"
}

variable "path_part" {
  description = "API path part"
}

variable "http_method" {
  description = "HTTP method"
}

variable "authorization" {
  description = "API authorization type"
}

variable "status_code" {
  description = "Status code for response"
}

variable "integration_type" {
  description = "API integration type"
}

variable "lambda_arn" {
  description = "ARN of Lambda function"
}

variable "lambda_invoke_arn" {
  description = "Lambda Invoke ARN"
}

variable "stage_name" {
  description = "API stage name"
}

variable "lambda_function_name" {
  description = "Lambda function name"
}

variable "authorizer_name" {
  description = "Name of API authorizer"
}

variable "auth_type" {
  description = "Type of API authorizer"
}

variable "provider_arn" {
  description = "ARN of Cognito User pool"
}

variable "authorization_type" {
  description = "Type of authorizer"
}

variable "authorization_provider" {
  description = "ID of authorizer"
}