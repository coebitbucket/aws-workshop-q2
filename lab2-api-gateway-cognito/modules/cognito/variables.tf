variable "pool_name" {
  description = "Cognito pool name"
}

variable "pool_app_name" {
  description = "Cognito application client name"
}