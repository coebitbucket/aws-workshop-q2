variable "name" {
  description = "Name for Lamdba function"
}

variable "s3bucket" {
  description = "S3 bucket name with source code"
}

variable "appversion" {
  description = "Key of application in S3"
}

variable "handler" {
  description = "Handler of application"
}

variable "runtime" {
  description = "Runtime type"
}

variable "iam_name" {
  description = "IAM Role name"
}