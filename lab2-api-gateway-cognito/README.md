# aws-agw-workshop

How to run

1) terraform init -plugin-dir ".terraform/plugins/darwin_amd64/"
2) terraform plan
3) terraform apply

Please note, to resolve issue https://github.com/terraform-providers/terraform-provider-aws/issues/10958 we use custom AWS provider with fixes
