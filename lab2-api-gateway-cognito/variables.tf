variable "lambda_name" {
  default = "weather-app"
}

variable "region" {
  description = "AWS Region"
  default     = "us-east-1"
}

variable "lambda_s3" {
  description = "S3 bucket with source code"
  default     = "my-private-bucket-for-serverless-app"
}

variable "lambda_application" {
  description = "S3 key of application to deploy"
  default     = "weather-app-1-0-3.zip"
}

variable "lambda_handler" {
  description = "App handler name"
  default     = "weather-app"
}

variable "lambda_runtime" {
  description = "Runtime type"
  default     = "go1.x"
}

variable "lambda_iam_role" {
  description = "Name for Lambda IAM Role"
  default     = "weather-app-role"
}

variable "api_name" {
  description = "API endpoint name"
  default     = "weather-api"
}

variable "api_description" {
  description = "API gateway description"
  default     = "Lambda Weather via API Gateway"
}

variable "api_model_name" {
  description = "Gateway model name"
  default     = "request"
}

variable "api_path_part" {
  description = "API path part in URL"
  default     = "v1"
}

variable "api_http_method" {
  description = "API http method"
  default     = "POST"
}

variable "api_auth" {
  description = "API authorization type"
  default     = "NONE"
}

variable "api_status_code" {
  description = "Response status code"
  default     = "200"
}

variable "api_integration" {
  description = "API integration type"
  default     = "AWS"
}

variable "api_stage_name" {
  description = "API stage name"
  default     = "stage"
}

variable "api_authorizer_name" {
  description = "API authorizer name"
  default     = "api-cognito-authorizer"
}

variable "cognito_pool_name" {
  description = "Cognito User Pool Name"
  default     = "weather-app-cognito-pool"
}
variable "cognito_pool_app_name" {
  description = "Cognito User Pool Application name"
  default     = "client"
}

variable "cognito_auth_type" {
  description = "Type of API authorizer"
  default     = "COGNITO_USER_POOLS"
}
