terraform {
  required_version = "~>0.12"
}

provider "aws" {
  region = var.region
}

module "lambda" {
  source     = "./modules/lambda"
  name       = var.lambda_name
  s3bucket   = var.lambda_s3
  appversion = var.lambda_application
  handler    = var.lambda_handler
  runtime    = var.lambda_runtime
  iam_name   = var.lambda_iam_role

}

module "apigw" {
  source                 = "./modules/agw"
  name                   = var.api_name
  description            = var.api_description
  path_part              = var.api_path_part
  model_name             = var.api_model_name
  http_method            = var.api_http_method
  authorization          = var.api_auth
  status_code            = var.api_status_code
  integration_type       = var.api_integration
  lambda_arn             = module.lambda.arn
  lambda_invoke_arn      = module.lambda.invoke_arn
  stage_name             = var.api_stage_name
  lambda_function_name   = module.lambda.name
  authorizer_name        = var.api_authorizer_name
  auth_type              = var.cognito_auth_type
  provider_arn           = module.cognitoauth.cognito_user_pool_arn
  authorization_type     = "COGNITO_USER_POOLS"
  authorization_provider = module.apigw.authorizer_id
}

module "cognitoauth" {
  source        = "./modules/cognito"
  pool_name     = var.cognito_pool_name
  pool_app_name = var.cognito_pool_app_name
}
