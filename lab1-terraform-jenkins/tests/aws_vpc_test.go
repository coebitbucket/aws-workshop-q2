package test

import (
	"fmt"
	"testing"
	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/gruntwork-io/terratest/modules/environment"
	aws_orig "github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/stretchr/testify/assert"
)

// An example of how to test the Terraform module in examples/terraform-aws-network-example using Terratest.
func TestVpcWorkshopSandbox(t *testing.T) {
	t.Parallel()

	vpcCidrExpected := "192.168.100.0/24"
	stateBucket := environment.GetFirstNonEmptyEnvVarOrFatal(t, []string{"STATE_BUCKET"})
	stateBucketRegion := environment.GetFirstNonEmptyEnvVarOrFatal(t, []string{"STATE_BUCKET_REGION"})
	awsTestRegion := environment.GetFirstNonEmptyEnvVarOrFatal(t, []string{"TESTS_REGION"})

	terraformOptions := &terraform.Options{
		TerraformDir: "../demo-resources",

		Vars: map[string]interface{}{
			"aws_region":          awsTestRegion,
		},
		
		BackendConfig: map[string]interface{}{
			"bucket": stateBucket,
			"key": "tests/lab1-terraform-jenkins/demo-resources",
			"region": stateBucketRegion,
		},
	}

	// At the end of the test, run `terraform destroy` to clean up any resources that were created
	defer terraform.Destroy(t, terraformOptions)

	// This will run `terraform init` and `terraform apply` and fail the test if there are any errors
	terraform.InitAndApply(t, terraformOptions)

	// Run `terraform output` to get the value of an output variable
	vpcCIDR := terraform.Output(t, terraformOptions, "main_vpc_cidr")

	assert.Equal(t, vpcCIDR, vpcCidrExpected)
}

func TestVpcWorkshopProd(t *testing.T) {
	t.Parallel()

	awsRegion := environment.GetFirstNonEmptyEnvVarOrFatal(t, []string{"STATE_BUCKET_REGION"})
	
	vpcFilter := []*ec2.Filter{
		&ec2.Filter{
			Name: aws_orig.String("tag:Name"),
			Values: []*string{
				aws_orig.String(fmt.Sprintf("test VPC in %s region for aws-workshop-q2 Demo", awsRegion)),
			},
		},
	}

	vpc, _ := aws.GetVpcsE(t, vpcFilter, awsRegion)
	assert.NotEmpty(t, vpc[0].Id)
}
