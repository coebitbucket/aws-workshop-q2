module vpc

go 1.13

require (
	github.com/aws/aws-sdk-go v1.27.3
	github.com/gruntwork-io/terratest v0.23.1
	github.com/stretchr/testify v1.4.0
)
