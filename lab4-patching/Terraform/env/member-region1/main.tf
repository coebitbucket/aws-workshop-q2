# Configure the AWS Provider
provider "aws" {
  region  = "us-east-1"
  profile = "ssmlab-member"
  #region      = "${var.aws_region}"
  #profile     = "${var.aws_profile}"
}

terraform {
  backend "s3" {
    bucket     = "ssm-lab-master-terraform-state-member"
    key        = "region1/terraform.tfstate"
    region     = "us-east-1"
    profile = "ssmlab-member"
  }
}

module "vpc" {
  source          = "../../modules/vpc"
  aws_region      = "${var.aws_region}"
  env_suffix      = "${var.env}"
  project         = "${var.project}"

  vpc_name = "${var.vpc_name}"
  vpc_cidr = "${var.vpc_cidr}"

  azs              = "${var.azs}"
  public_subnets   = "${var.public_subnets}"
  private_subnets  = "${var.private_subnets}"

  enable_dns_hostnames = "${var.enable_dns_hostnames}"
  enable_dns_support   = "${var.enable_dns_support}"

  #
  #  enable_dhcp_options              = true
  #  dhcp_options_domain_name         = "service.consul"
  #  dhcp_options_domain_name_servers = ["127.0.0.1", "10.10.0.2"]

  tags = {
    Owner       = "SSM-Lab"
    Environment = "test"
  }
}

module "ssm_endpoints" {
  source            = "../../modules/ssm_endpoints"
  aws_region        = "${var.aws_region}"
  aws_bucket_region = "${var.aws_bucket_region}"
  env_suffix        = "${var.env}"
  project           = "${var.project}"

  ssm_vpc_id                    = module.vpc.vpc_id
  ssm_vpc_cidr_block            = module.vpc.vpc_cidr_block
  ssm_endpoints_private_subnets = module.vpc.private_subnets

  # Enable VPC Endpoints
  enable_ssm_endpoint                       = "${var.enable_ssm_endpoint}"
  enable_ssmmessages_endpoint               = "${var.enable_ssmmessages_endpoint}"
  ssm_endpoint_private_dns_enabled          = "${var.ssm_endpoint_private_dns_enabled}"
  ssmmessages_endpoint_private_dns_enabled  = "${var.ssmmessages_endpoint_private_dns_enabled}"
  enable_ec2messages_endpoint               = "${var.enable_ec2messages_endpoint}"
  ec2messages_endpoint_private_dns_enabled  = "${var.ec2messages_endpoint_private_dns_enabled}"

  tags = {
    Owner       = "SSM-Lab"
    Environment = "test"
  }
}

module "iam" {
  source            = "../../modules/iam"
  aws_region        = "${var.aws_region}"
  env_suffix        = "${var.env}"
  project           = "${var.project}"
}

module "ssm_execution_role" {
  source            = "../../modules/ssm_execution_role"
  aws_region        = "${var.aws_region}"
  env_suffix        = "${var.env}"
  project           = "${var.project}"
}

module "resource_groups" {
  source            = "../../modules/resource_groups"
  aws_region        = "${var.aws_region}"
  env_suffix        = "${var.env}"
  project           = "${var.project}"
}

module "ec2" {
  source                = "../../modules/ec2"
  aws_region            = "${var.aws_region}"
  env_suffix            = "${var.env}"
  project               = "${var.project}"
  instance_type         = "${var.instance_type}"
  ec2_vpc_id            = module.vpc.vpc_id
  ec2_vpc_cidr_block    = module.vpc.vpc_cidr_block
  ec2_subnet_id         = module.vpc.first_private_subnet

  iam_instance_profile  = module.iam.ec2_iam_profile_name
}
