#####################################################
# General outputs
#####################################################
output "aws_region" {
description = "AWS region to deploy resources in"
  value = "${var.aws_region}"
}

output "aws_profile" {
description = "AWS CLI profile"
  value = "${var.aws_profile}"
}

output "env" {
  description = "Environment: prod | test | stage | demo | dev | preprod"
  value = "${var.env}"
}

output "project" {
  description = "Project name"
  value = "${var.project}"
}

#####################################################
# Networking outputs
#####################################################

output "vpc_id" {
  description = "The ID of the VPC"
  value       = "${module.vpc.vpc_id}"
}

output "vpc_arn" {
  description = "The ARN of the VPC"
  value       = "${module.vpc.vpc_arn}"
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  value       = "${module.vpc.vpc_cidr_block}"
}

output "vpc_instance_tenancy" {
  description = "Tenancy of instances spin up within VPC"
  value       = "${module.vpc.vpc_instance_tenancy}"
}

output "vpc_enable_dns_support" {
  description = "Whether or not the VPC has DNS support"
  value       = "${module.vpc.vpc_enable_dns_support}"
}

output "vpc_enable_dns_hostnames" {
  description = "Whether or not the VPC has DNS hostname support"
  value       = "${module.vpc.vpc_enable_dns_hostnames}"
}

output "vpc_main_route_table_id" {
  description = "The ID of the main route table associated with this VPC"
  value       = "${module.vpc.vpc_main_route_table_id}"
}

output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = "${module.vpc.public_subnets}"
}

output "public_subnet_arns" {
  description = "List of ARNs of public subnets"
  value       = "${module.vpc.public_subnet_arns}"
}

output "public_subnets_cidr_blocks" {
  description = "List of cidr_blocks of public subnets"
  value       = "${module.vpc.public_subnets_cidr_blocks}"
}

output "private_subnets" {
  description = "List of IDs of public subnets"
  value       = "${module.vpc.private_subnets}"
}

output "first_private_subnet" {
  description = "IDs of first private subnets"
  value       = "${module.vpc.first_private_subnet}"
}

output "private_subnet_arns" {
  description = "List of ARNs of public subnets"
  value       = "${module.vpc.public_subnet_arns}"
}

output "private_subnets_cidr_blocks" {
  description = "List of cidr_blocks of public subnets"
  value       = "${module.vpc.public_subnets_cidr_blocks}"
}

#####################################################
# SSM endpoints outputs
#####################################################
output "ssm_endpoints_sg_id" {
  description = "Security Group for communication with SSM"
  value       = "${module.ssm_endpoints.ssm_endpoints_sg_id}"
}
