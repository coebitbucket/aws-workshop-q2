#####################################################
# General variables
#####################################################
aws_region    = "us-east-1"
aws_bucket_region    = "us-east-1"
aws_profile		= "ssmlab-master"
env           = "test1"
project       = "lab"

#####################################################
# Networking variables
#####################################################

vpc_name = "ssmlab-vpc"
vpc_cidr = "10.10.0.0/16"

azs               = ["us-east-1a", "us-east-1b"]
public_subnets    = ["10.10.1.0/24", "10.10.2.0/24"]
private_subnets   = ["10.10.11.0/24", "10.10.12.0/24"]

enable_dns_hostnames = true
enable_dns_support   = true

#####################################################
# SGs variables
#####################################################
create_ssh_sgs  = true
create_rdp_sgs  = false

#####################################################
# SSM endpoints variables
#####################################################
# Enable VPC Endpoints
enable_ssm_endpoint                       = true
enable_ssmmessages_endpoint               = true
ssm_endpoint_private_dns_enabled          = true
ssmmessages_endpoint_private_dns_enabled  = true
enable_ec2messages_endpoint               = true
ec2messages_endpoint_private_dns_enabled  = true

#####################################################
# IAM variables
#####################################################

#####################################################
# EC2 instance variables
#####################################################
instance_type          = "t3.small"
key_name              = "ssm-lab-key"
root_block_device  = [{
    volume_size           = 8
    volume_type           = "standard"
}]
