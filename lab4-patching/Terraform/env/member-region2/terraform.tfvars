#####################################################
# General variables
#####################################################
aws_region    = "us-west-2"
aws_bucket_region    = "us-east-1"
aws_profile		= "ssmlab-member"
env           = "test4"
project       = "lab"

#####################################################
# Networking variables
#####################################################

vpc_name = "ssmlab-vpc"
vpc_cidr = "10.20.0.0/16"

azs               = ["us-west-2a", "us-west-2b"]
public_subnets    = ["10.20.1.0/24", "10.20.2.0/24"]
private_subnets   = ["10.20.11.0/24", "10.20.12.0/24"]

enable_dns_hostnames = true
enable_dns_support   = true

#####################################################
# SSM endpoints variables
#####################################################
# Enable VPC Endpoints
enable_ssm_endpoint                       = true
enable_ssmmessages_endpoint               = true
ssm_endpoint_private_dns_enabled          = true
ssmmessages_endpoint_private_dns_enabled  = true
enable_ec2messages_endpoint               = true
ec2messages_endpoint_private_dns_enabled  = true

#####################################################
# IAM variables
#####################################################


#####################################################
# EC2 instance variables
#####################################################
instance_type          = "t3.small"
key_name              = "ssm-lab-key"
root_block_device  = [{
    volume_size           = 8
    volume_type           = "standard"
}]
