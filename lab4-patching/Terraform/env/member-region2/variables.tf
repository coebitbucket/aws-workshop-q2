#####################################################
# General variables
#####################################################
variable "aws_profile" {
  description = "AWS CLI profile"
}

variable "aws_region" {
  description = "AWS region to deploy resources in"
}

variable "aws_bucket_region" {
  description = "AWS region for S3 bucket with Terraform state file"
}

variable "env" {
  description = "Suffix to append to all recources names. env: prod | test | stage | demo | dev | preprod"
}

variable "project" {
  description = "Project name"
}

#####################################################
# Networking variables
#####################################################

variable "vpc_name" {
  description = "VPC Name"
}

variable "vpc_cidr" {
  description = "VPC CIDR for environment"
}

variable "azs" {
  description = "A list of availability zones in the region"
  default     = []
}

variable "public_subnets" {
  description = "A list of public subnets inside the VPC"
  default     = []
}

variable "private_subnets" {
  description = "A list of private subnets inside the VPC"
  default     = []
}

variable "enable_dns_hostnames" {
  description = "Should be true to enable DNS hostnames in the VPC"
}

variable "enable_dns_support" {
  description = "Should be true to enable DNS support in the VPC"
}

#####################################################
# SGs variables
#####################################################
variable "create_ssh_sgs" {
  description = "Controls if SSH security groups must be created"
  type        = bool
  default     = true
}

variable "create_rdp_sgs" {
  description = "Controls if RDP security groups must be created"
  type        = bool
  default     = false
}

variable "sg_vpc_id" {
  description = "The ID of the VPC"
  type        = string
  default     = ""
}

#####################################################
# SSM endpoints variables
#####################################################
variable "ssm_vpc_id" {
  description = "The ID of the VPC"
  type        = string
  default     = ""
}

variable "ssm_vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  type        = string
  default     = ""
}

variable "ssm_endpoints_private_subnets" {
  description = "List of IDs of private subnets with SSM endpoints"
  type        = list(string)
  default     = []
}

variable "enable_ssm_endpoint" {
  description = "Should be true if you want to provision an SSM endpoint to the VPC"
  type        = bool
  default     = false
}

variable "enable_ssmmessages_endpoint" {
  description = "Should be true if you want to provision a SSMMESSAGES endpoint to the VPC"
  type        = bool
  default     = false
}

variable "ssm_endpoint_private_dns_enabled" {
  description = "Whether or not to associate a private hosted zone with the specified VPC for SSM endpoint"
  type        = bool
  default     = false
}

variable "ssmmessages_endpoint_private_dns_enabled" {
  description = "Whether or not to associate a private hosted zone with the specified VPC for SSMMESSAGES endpoint"
  type        = bool
  default     = false
}

variable "enable_ec2_endpoint" {
  description = "Should be true if you want to provision an EC2 endpoint to the VPC"
  type        = bool
  default     = false
}

variable "ec2_endpoint_private_dns_enabled" {
  description = "Whether or not to associate a private hosted zone with the specified VPC for EC2 endpoint"
  type        = bool
  default     = false
}

variable "enable_ec2messages_endpoint" {
  description = "Should be true if you want to provision an EC2MESSAGES endpoint to the VPC"
  type        = bool
  default     = false
}

variable "ec2messages_endpoint_private_dns_enabled" {
  description = "Whether or not to associate a private hosted zone with the specified VPC for EC2MESSAGES endpoint"
  type        = bool
  default     = false
}

#####################################################
# IAM variables
#####################################################
variable "create_ssm_automation_administration_role" {
  description = "Choose if create AWS-SystemsManager-AutomationAdministrationRole roles"
  type        = bool
  default     = true
}

variable "create_ssm_automation_execution_role" {
  description = "Choose if create AWS-SystemsManager-AutomationExecutionRole roles"
  type        = bool
  default     = false
}
#####################################################
# EC2 instance variables
#####################################################
variable "instance_type" {
  description = "The type of instance to start"
  type        = string
}

variable "ec2_vpc_id" {
  description = "The VPC ID to launch instance in"
  type        = string
  default     = ""
}

variable "ec2_vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  type        = string
  default     = ""
}

variable "ec2_subnet_id" {
  description = "The VPC Subnet ID to launch in"
  type        = string
  default     = ""
}

variable "key_name" {
  description = "The key name to use for the instance"
  type        = string
  default     = "ssm-lab-key"
}

#variable "ami" {
#  description = "ID of AMI to use for the instance"
#  type        = string
#}

variable "root_block_device" {
  description = "Customize details about the root block device of the instance. See Block Devices below for details"
  type        = list(map(string))
  default     = []
}

variable "iam_instance_profile" {
  description = "The IAM Instance Profile to launch the instance with. Specified as the name of the Instance Profile."
  type        = string
  default     = ""
}
