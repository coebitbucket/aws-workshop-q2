terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region  = "${var.aws_region}"
}

terraform {
  backend "s3" {}
}

#####################################################
# Administrator automation IAM role
#####################################################
resource "aws_iam_role" "ssm_automation_administration_role_this" {

  name = "AWS-SystemsManager-AutomationAdministrationRole"
  #path = "/service-role/"
  path = "/"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ssm.amazonaws.com"
      },
      "Action": [
        "sts:AssumeRole"
      ]
    }
  ]
}
EOF
tags = {
  "Name" = "AWS-SystemsManager-AutomationAdministrationRole"
  }
}

#####################################################
# Automation Execution Policy
#####################################################
resource "aws_iam_policy" "ssm_automation_administration_policy_this" {
  #count = var.create_ssm_automation_administration_role ? 1 : 0
  name = "AWS-SystemsManager-AutomationExecutionPolicy"
  path = "/"
  description = "Configure the AWS-SystemsManager-AutomationAdministrationRole to enable use of AWS Systems Manager Cross Account/Region Automation execution"
  policy = <<EOF
{
      "Version": "2012-10-17",
      "Statement": [
          {
              "Action": [
                  "sts:AssumeRole"
              ],
              "Resource": "arn:aws:iam::*:role/AWS-SystemsManager-AutomationExecutionRole",
              "Effect": "Allow"
          },
          {
              "Action": [
                  "organizations:ListAccountsForParent"
              ],
              "Resource": [
                  "*"
              ],
              "Effect": "Allow"
          }
      ]
}
EOF
}

#####################################################
# Policy to Role attachement
#####################################################
resource "aws_iam_role_policy_attachment" "ssm_service_role_policy_attachment_this" {
  #count = var.create_ssm_automation_administration_role ? 1 : 0
  role       = "${aws_iam_role.ssm_automation_administration_role_this.name}"
  policy_arn = "${aws_iam_policy.ssm_automation_administration_policy_this.arn}"
}

#####################################################
# Automation Execution IAM role
#####################################################
resource "aws_iam_role" "ssm_automation_execution_role_this" {

  name = "AWS-SystemsManager-AutomationExecutionRole"
  #path = "/service-role/"
  path = "/"
  assume_role_policy = <<EOF
{
      "Version": "2012-10-17",
      "Statement": [
          {
              "Effect": "Allow",
              "Principal": {
                  "AWS": "*"
              },
              "Action": [
                  "sts:AssumeRole"
              ]
          },
          {
              "Effect": "Allow",
              "Principal": {
                  "Service": "ssm.amazonaws.com"
              },
              "Action": [
                  "sts:AssumeRole"
              ]
          }
      ]
}
EOF
tags = {
  "Name" = "AWS-SystemsManager-AutomationExecutionRole"
  }
}

#####################################################
# Automation Execution Policy
#####################################################
resource "aws_iam_policy" "ssm_automation_execution_policy_this" {
  #count = var.create_ssm_automation_administration_role ? 1 : 0
  name = "SSM-ExecutionPolicy"
  path = "/"

  policy = <<EOF
{
      "Version": "2012-10-17",
      "Statement": [
          {
              "Action": [
                  "resource-groups:ListGroupResources",
                  "tag:GetResources"
              ],
              "Resource": "*",
              "Effect": "Allow"
          },
          {
              "Action": [
                  "iam:PassRole"
              ],
              "Resource": "arn:aws:iam::*:role/AWS-SystemsManager-AutomationExecutionRole",
              "Effect": "Allow"
          }
      ]
}
EOF
}

#####################################################
# AWS Managed Policy
#####################################################
#data "aws_iam_policy" "AmazonSSMAutomationRole" {
#  arn = "arn:aws:iam::aws:policy/service-role/AmazonSSMAutomationRole"
#}

#####################################################
# Policy to Role attachement
#####################################################
resource "aws_iam_role_policy_attachment" "ssm_execution_role_policy_attachment_this" {
  role       = "${aws_iam_role.ssm_automation_execution_role_this.name}"
  policy_arn = "${aws_iam_policy.ssm_automation_execution_policy_this.arn}"
}

resource "aws_iam_role_policy_attachment" "ssm_execution_role_policy_attachment_this2" {
  role       = "${aws_iam_role.ssm_automation_execution_role_this.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonSSMAutomationRole"
}

resource "aws_iam_role_policy_attachment" "ssm_execution_role_policy_attachment_this3" {
  role       = "${aws_iam_role.ssm_automation_execution_role_this.name}"
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}
