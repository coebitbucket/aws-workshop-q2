terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region  = "${var.aws_region}"
}

terraform {
  backend "s3" {}
}

#####################################################
# Resource Group needed for Systems Manager
#####################################################
resource "aws_resourcegroups_group" "ssm_resource_group" {
  #name = "SSM-patching"
  name = format("%s-ssm-patching", var.project)

  resource_query {
    query = <<JSON
{
  "ResourceTypeFilters": [
    "AWS::EC2::Instance"
  ],
  "TagFilters": [
    {
      "Key": "SSMpatching",
      "Values": ["True"]
    }
  ]
}
JSON
  }
}
