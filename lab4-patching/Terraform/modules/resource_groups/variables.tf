variable "aws_region" {
  type = string
  default = ""
}

##############################################
# Variables that help to form resources names
##############################################
variable "env_suffix" {
  description = "Suffix to append to all recources names. env: prod | test | stage | demo | dev | preprod"
  type        = string
  default     = "test"
}

variable "project" {
  description = "Project name"
  type        = string
  default     = "lab"
}
