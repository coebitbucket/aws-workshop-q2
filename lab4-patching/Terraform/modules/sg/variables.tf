##############################################
# Global variables
##############################################
variable "aws_region" {
  type = string
  default = "us-east-1"
}

##############################################
# Variables that help to form resources names
##############################################
variable "env_suffix" {
  description = "Suffix to append to all recources names. env: prod | test | stage | demo | dev | preprod"
  type        = string
  default     = "test"
}

variable "project" {
  description = "Project name"
  type        = string
  default     = "lab1"
}

##############################################
# Variables that enable Security Groups creation
##############################################
variable "create_ssh_sgs" {
  description = "Controls if SSH security groups must be created"
  type        = bool
  default     = true
}

variable "create_rdp_sgs" {
  description = "Controls if RDP security groups must be created"
  type        = bool
  default     = false
}

##############################################
# Network Variables
##############################################
variable "sg_vpc_id" {
  description = "The VPC ID to deploy SG in"
  type        = string
  default     = ""
}

##############################################
# Tags
##############################################
variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}
