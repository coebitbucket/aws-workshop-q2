#######################
# SG
#######################
terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region  = "${var.aws_region}"
}

terraform {
  backend "s3" {}
}

####################################
# SSH
####################################
resource "aws_security_group" "ssh_access" {
  count = var.create_ssh_sgs ? 1 : 0
  name        = format("%s-%s-ssh-security-group", var.env_suffix, var.project)
  description = "Allow ssh inbound traffic"
  vpc_id      = var.sg_vpc_id
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags = merge(
    {
      "Name" = format("%s-%s-ssh-security-group", var.env_suffix, var.project)
    },
    var.tags
  )
}

####################################
# RDP
####################################
resource "aws_security_group" "rdp_access" {
  count = var.create_rdp_sgs ? 1 : 0
  name        = format("%s-%s-rdp-security-group", var.env_suffix, var.project)
  description = "Allow ssh inbound traffic"
  vpc_id      = var.sg_vpc_id
  ingress {
    description = "RDP"
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags = merge(
    {
      "Name" = format("%s-%s-rdp-security-group", var.env_suffix, var.project)
    },
    var.tags
  )
}
