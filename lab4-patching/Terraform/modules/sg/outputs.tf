output "ssh_sg_id" {
  value = aws_security_group.ssh_access.*.id
}

output "rdp_sg_id" {
  value = aws_security_group.ssh_access.*.id
}
