terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region  = "${var.aws_region}"
}

terraform {
  backend "s3" {}
}

#####################################################
# Common
#####################################################
data "aws_iam_policy" "AmazonEC2RoleforSSM" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}

#####################################################
# Instance Profile
#####################################################
resource "aws_iam_instance_profile" "this" {
  name = format("%s-%s-instance-profile", var.env_suffix, var.project)
  role = "${aws_iam_role.ec2_role.name}"
}

resource "aws_iam_role" "ec2_role" {
  name = format("%s-%s-ec2-instance-role", var.env_suffix, var.project)
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "this" {
  role       = "${aws_iam_role.ec2_role.name}"
  policy_arn = "${data.aws_iam_policy.AmazonEC2RoleforSSM.arn}"
}
