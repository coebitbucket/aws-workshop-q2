#output "ssm_automation_administration_policy_arn" {
#  description = "AWS-SystemsManager-AutomationExecutionPolicy policy ARN"
#  value       = aws_iam_policy.ssm_automation_administration_policy_this.*.arn
#}

#output "ssm_automation_administration_role_arn" {
#  description = "AWS-SystemsManager-AutomationAdministrationRole role ARN"
#  value       = aws_iam_role.ssm_automation_administration_role_this.*.arn
#}

output "ec2_iam_profile_name" {
  description = "Name of the instance profile to use with SSM-managed instances"
  value       = aws_iam_instance_profile.this.name
}
