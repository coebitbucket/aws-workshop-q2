#####################################################
# Generic EC2 instance
#####################################################

terraform {
  required_version = "> 0.12.0"
}

provider "aws" {
  region  = "${var.aws_region}"
}

terraform {
  backend "s3" {}
}

####################################
# SSH
####################################
resource "aws_security_group" "this" {
  #count = var.create_ssh_sgs ? 1 : 0
  name        = format("%s-%s-security-group", var.env_suffix, var.project)
  description = "Allow ssh and SSM inbound traffic"
  vpc_id      = var.ec2_vpc_id
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Allow inbound access to SSM"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks     = ["${var.ec2_vpc_cidr_block}"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags = merge(
    {
      "Name" = format("%s-%s-security-group", var.env_suffix, var.project)
    },
    var.tags
  )
}

data "aws_ami" "amazon_linux" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name = "name"

    values = [
      "amzn-ami-hvm-*-x86_64-gp2",
    ]
  }

  filter {
    name = "owner-alias"

    values = [
      "amazon",
    ]
  }
}

resource "aws_instance" "this" {
  count = var.instance_count

  ami              = data.aws_ami.amazon_linux.id
  #ami = "ami-00068cd7555f543d5"
  instance_type    = var.instance_type
  user_data        = var.user_data
  user_data_base64 = var.user_data_base64
  #subnet_id = data.terraform_remote_state.vpc.outputs.private_subnets.ids[0]
  subnet_id = var.ec2_subnet_id
  #subnet_id = length(var.network_interface) > 0 ? null : element(
  #  distinct(compact(concat([var.subnet_id], var.subnet_ids))),
  #  count.index,
  #)
  key_name               = var.key_name
  monitoring             = var.monitoring
  get_password_data      = var.get_password_data
  #vpc_security_group_ids = var.vpc_security_group_ids
  vpc_security_group_ids = [aws_security_group.this.id]
  iam_instance_profile   = var.iam_instance_profile

  associate_public_ip_address = var.associate_public_ip_address
  private_ip                  = length(var.private_ips) > 0 ? element(var.private_ips, count.index) : var.private_ip
  ipv6_address_count          = var.ipv6_address_count
  ipv6_addresses              = var.ipv6_addresses

  ebs_optimized = var.ebs_optimized

  dynamic "root_block_device" {
    for_each = var.root_block_device
    content {
      delete_on_termination = lookup(root_block_device.value, "delete_on_termination", null)
      encrypted             = lookup(root_block_device.value, "encrypted", null)
      iops                  = lookup(root_block_device.value, "iops", null)
      kms_key_id            = lookup(root_block_device.value, "kms_key_id", null)
      volume_size           = lookup(root_block_device.value, "volume_size", null)
      volume_type           = lookup(root_block_device.value, "volume_type", null)
    }
  }

  dynamic "ebs_block_device" {
    for_each = var.ebs_block_device
    content {
      delete_on_termination = lookup(ebs_block_device.value, "delete_on_termination", null)
      device_name           = ebs_block_device.value.device_name
      encrypted             = lookup(ebs_block_device.value, "encrypted", null)
      iops                  = lookup(ebs_block_device.value, "iops", null)
      kms_key_id            = lookup(ebs_block_device.value, "kms_key_id", null)
      snapshot_id           = lookup(ebs_block_device.value, "snapshot_id", null)
      volume_size           = lookup(ebs_block_device.value, "volume_size", null)
      volume_type           = lookup(ebs_block_device.value, "volume_type", null)
    }
  }

  dynamic "ephemeral_block_device" {
    for_each = var.ephemeral_block_device
    content {
      device_name  = ephemeral_block_device.value.device_name
      no_device    = lookup(ephemeral_block_device.value, "no_device", null)
      virtual_name = lookup(ephemeral_block_device.value, "virtual_name", null)
    }
  }

  dynamic "network_interface" {
    for_each = var.network_interface
    content {
      device_index          = network_interface.value.device_index
      network_interface_id  = lookup(network_interface.value, "network_interface_id", null)
      delete_on_termination = lookup(network_interface.value, "delete_on_termination", false)
    }
  }

  source_dest_check                    = length(var.network_interface) > 0 ? null : var.source_dest_check
  disable_api_termination              = var.disable_api_termination
  instance_initiated_shutdown_behavior = var.instance_initiated_shutdown_behavior
  placement_group                      = var.placement_group
  tenancy                              = var.tenancy

  tags = merge(
    {
      "Name" = var.instance_count > 1 || var.use_num_suffix ? format("%s-%s-instance-%d", var.env_suffix, var.project, count.index + 1) : format("%s-%s-instance", var.env_suffix, var.project)
    },
    {
      "SSMpatching" = "True"
    },
    var.tags,
  )

  volume_tags = merge(
    {
      "Name" = var.instance_count > 1 || var.use_num_suffix ? format("%s-%s-volume-%d", var.env_suffix, var.project, count.index + 1) : format("%s-%s-volume", var.env_suffix, var.project)
    },
    var.volume_tags,
  )
}
