#####################################################
# VPC Endpoints needed for Systems Manager
#####################################################

terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region  = "${var.aws_region}"
}

terraform {
  backend "s3" {}
}

#######################
# VPC Endpoint for SSM
#######################
resource "aws_security_group" "ssm_endpoint_sg" {
  count = var.enable_ssm_endpoint ? 1 : 0

  name        = format("%s-%s-ssm-security-group", var.env_suffix, var.project)
  description = "Allow SSM inbound traffic"
  vpc_id      = "${var.ssm_vpc_id}"
  ingress {
    description = "Allow inbound access to SSM"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks     = ["${var.ssm_vpc_cidr_block}"]
  }

  tags = merge(
    {
      "Name" = format("%s-%s-ssm-security-group", var.env_suffix, var.project)
    },
    var.tags
  )
}

data "aws_vpc_endpoint_service" "ssm" {
  count = var.enable_ssm_endpoint ? 1 : 0

  service = "ssm"
}

resource "aws_vpc_endpoint" "ssm" {
  count = var.enable_ssm_endpoint ? 1 : 0

  vpc_id      = "${var.ssm_vpc_id}"
  service_name      = data.aws_vpc_endpoint_service.ssm[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.ssm_endpoint_sg[count.index].id]
  subnet_ids          = coalescelist(var.ssm_endpoint_subnet_ids, var.ssm_endpoints_private_subnets)

  private_dns_enabled = var.ssm_endpoint_private_dns_enabled

  tags = merge(
  {
    "Name" = format("%s-%s-ssm-endpoint", var.env_suffix, var.project)
  },
  var.tags,
  )
}

###############################
# VPC Endpoint for SSMMESSAGES
###############################
data "aws_vpc_endpoint_service" "ssmmessages" {
  count = var.enable_ssmmessages_endpoint ? 1 : 0

  service = "ssmmessages"
}

resource "aws_vpc_endpoint" "ssmmessages" {
  count = var.enable_ssmmessages_endpoint ? 1 : 0

  vpc_id      = "${var.ssm_vpc_id}"
  service_name      = data.aws_vpc_endpoint_service.ssmmessages[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.ssm_endpoint_sg[count.index].id]
  subnet_ids          = coalescelist(var.ssmmessages_endpoint_subnet_ids, var.ssm_endpoints_private_subnets)
  private_dns_enabled = var.ssmmessages_endpoint_private_dns_enabled

  tags = merge(
  {
    "Name" = format("%s-%s-ssmmessages-endpoint", var.env_suffix, var.project)
  },
  var.tags,
  )
}

#######################
# VPC Endpoint for EC2
#######################
data "aws_vpc_endpoint_service" "ec2" {
  count = var.enable_ec2_endpoint ? 1 : 0

  service = "ec2"
}

resource "aws_vpc_endpoint" "ec2" {
  count = var.enable_ec2_endpoint ? 1 : 0

  vpc_id      = "${var.ssm_vpc_id}"
  service_name      = data.aws_vpc_endpoint_service.ec2[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = var.ec2_endpoint_security_group_ids
  subnet_ids          = coalescelist(var.ec2_endpoint_subnet_ids, var.ssm_endpoints_private_subnets)
  private_dns_enabled = var.ec2_endpoint_private_dns_enabled
}

###############################
# VPC Endpoint for EC2MESSAGES
###############################
data "aws_vpc_endpoint_service" "ec2messages" {
  count = var.enable_ec2messages_endpoint ? 1 : 0

  service = "ec2messages"
}

resource "aws_vpc_endpoint" "ec2messages" {
  count = var.enable_ec2messages_endpoint ? 1 : 0

  vpc_id      = "${var.ssm_vpc_id}"
  service_name      = data.aws_vpc_endpoint_service.ec2messages[0].service_name
  vpc_endpoint_type = "Interface"

  security_group_ids  = [aws_security_group.ssm_endpoint_sg[count.index].id]
  subnet_ids          = coalescelist(var.ec2messages_endpoint_subnet_ids, var.ssm_endpoints_private_subnets)
  private_dns_enabled = var.ec2messages_endpoint_private_dns_enabled

  tags = merge(
  {
    "Name" = format("%s-%s-ec2messages-endpoint", var.env_suffix, var.project)
  },
  var.tags,
  )
}
