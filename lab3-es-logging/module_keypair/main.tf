resource "null_resource" "keypair" {
    provisioner "local-exec" {
        command = "aws ec2 create-key-pair --region=${var.region} --key-name=${var.keypair_name}"
    }
}