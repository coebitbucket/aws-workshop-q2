data "aws_caller_identity" "current" {}

resource "aws_cognito_user_pool" "es_cognito_user_pool" {
  name = "upool_${var.unique_string}"
  admin_create_user_config {
      allow_admin_create_user_only = true
  }
}

resource "aws_cognito_user_pool_domain" "userpool_domain" {
    domain       = "authservice-${var.unique_string}"
    user_pool_id = aws_cognito_user_pool.es_cognito_user_pool.id
}

resource "aws_cognito_identity_pool" "es_cognito_identity_pool" {
    identity_pool_name = "idpool_${var.unique_string}"
    allow_unauthenticated_identities = false
    lifecycle {
      ignore_changes = [cognito_identity_providers]
    }  
}

resource "aws_iam_role" "unauthenticated" {
  name = "cognito_unauthenticated_role_${var.unique_string}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "cognito-identity.amazonaws.com"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "cognito-identity.amazonaws.com:aud": "${aws_cognito_identity_pool.es_cognito_identity_pool.id}"
        },
        "ForAnyValue:StringLike": {
          "cognito-identity.amazonaws.com:amr": "unauthenticated"
        }
      }
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "unauthenticated" {
  name = "cognito_unauthenticated__policy_${var.unique_string}"
  role = aws_iam_role.unauthenticated.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "mobileanalytics:PutEvents",
        "cognito-sync:*"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_role" "authenticated" {
  name = "cognito_authenticated_role_${var.unique_string}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "cognito-identity.amazonaws.com"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "cognito-identity.amazonaws.com:aud":"${aws_cognito_identity_pool.es_cognito_identity_pool.id}"
        },
        "ForAnyValue:StringLike": {
          "cognito-identity.amazonaws.com:amr":"authenticated"
        }
      }
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "authenticated" {
  name = "cognito_authenticated_policy_${var.unique_string}"
  role = aws_iam_role.authenticated.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "mobileanalytics:PutEvents",
        "cognito-sync:*",
        "cognito-identity:*",
        "es:*"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_role" "es_service_role" {
  name = "es_service_role_${var.unique_string}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "es.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "es_service_policy" {
  name = "es_service_policy_${var.unique_string}"
  role = aws_iam_role.es_service_role.id
  policy =<<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "iam:GetRole",
        "iam:PassRole",
        "iam:CreateRole",
        "iam:AttachRolePolicy",
        "ec2:DescribeVpcs",
        "cognito-idp:DescribeUserPool",
        "cognito-idp:CreateUserPoolClient",
        "cognito-idp:DeleteUserPoolClient",
        "cognito-idp:DescribeUserPoolClient",
        "cognito-idp:AdminInitiateAuth",
        "cognito-idp:AdminUserGlobalSignOut",
        "cognito-idp:ListUserPoolClients",
        "cognito-identity:DescribeIdentityPool",
        "cognito-identity:UpdateIdentityPool",
        "cognito-identity:SetIdentityPoolRoles",
        "cognito-identity:GetIdentityPoolRoles"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_cognito_identity_pool_roles_attachment" "main" {
  identity_pool_id = aws_cognito_identity_pool.es_cognito_identity_pool.id

  roles = {
    "authenticated" = "${aws_iam_role.authenticated.arn}",
    "unauthenticated" = "${aws_iam_role.unauthenticated.arn}"
  }
}


resource "aws_security_group" "es" {
  name        = "elasticsearch-sg-${var.unique_string}"
  description = "Managed by Terraform"
  vpc_id      = var.vpc_id
  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}

resource "null_resource" "waiter" {
  depends_on = [aws_iam_role_policy.es_service_policy, aws_iam_role.es_service_role]
  provisioner "local-exec" {
    command = "sleep 3"
  }
}

resource "aws_elasticsearch_domain" "elastic" {
  depends_on = [null_resource.waiter]
  domain_name = "es-${var.unique_string}"
  elasticsearch_version = var.es_version
 
  vpc_options {
    subnet_ids = [var.vpc_publicsubnet_id]
    security_group_ids = [aws_security_group.es.id]
  }

  cognito_options {
      enabled = true
      user_pool_id = aws_cognito_user_pool.es_cognito_user_pool.id
      identity_pool_id = aws_cognito_identity_pool.es_cognito_identity_pool.id
      role_arn = aws_iam_role.es_service_role.arn
  }

  cluster_config {
      instance_type = var.es_instance_type
      instance_count = 1
      dedicated_master_enabled = false
      zone_awareness_enabled = false
  }

  ebs_options {
      ebs_enabled = true
      volume_size = 10
  }

  access_policies = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "es:*",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                  "${aws_iam_role.authenticated.arn}",
                  "${var.iam_role_es_log_writer}"
                  ]
                },
            "Resource": "arn:aws:es:${var.region}:${data.aws_caller_identity.current.account_id}:domain/es-${var.unique_string}/*"
        }
    ]
}
EOF
}
