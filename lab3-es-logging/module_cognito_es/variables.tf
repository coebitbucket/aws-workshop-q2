variable "region" {}

variable "es_version" {}

variable "es_instance_type" {}

variable "vpc_id" {}
variable "vpc_publicsubnet_id" {}

variable "iam_role_es_log_writer" {}

variable "unique_string" {}

