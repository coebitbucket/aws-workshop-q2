output "es_arn" {
  value = aws_elasticsearch_domain.elastic.arn
}

output "es_id" {
  value = aws_elasticsearch_domain.elastic.domain_id
} 

output "es_endpoint" {
  value = aws_elasticsearch_domain.elastic.endpoint
}

output "userpool_domain" {
  value =  aws_cognito_user_pool_domain.userpool_domain.domain
}

