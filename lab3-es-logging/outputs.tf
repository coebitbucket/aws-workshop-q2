output "es_arn" {
  description = "arn of ES cluster"
  value = module.es_cognito.es_arn
}

output "es_id" {
  description = "id of ES cluster"
  value = module.es_cognito.es_id
}

output "es_endpoint" {
  description = "Domain-specific endpoint used to submit index, search, and data upload requests"
  value = module.es_cognito.es_endpoint
}

output "proxy_dns" {
  description = "pub dns name for proxy"
  value =  module.ec2_reverse_proxy.proxy_dns
}

output "monitored_instance_dns" {
  description = "pub dns name for monitored instance"
  value =  module.ec2_log_sender.monitored_instance
}