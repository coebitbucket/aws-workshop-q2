#THIS MODULE DEPLOYS EC2 INSTANCE THAT IS PUSHING ITS SYSLOG LOGS INTO CLOUDWATCH
data "aws_caller_identity" "current" {}

resource "aws_security_group" "log_sender_sg" {
  name        = "log-sender-sg"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_iam_role" "log_sender_role" {
  name = "log_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Principal": {"Service": "ec2.amazonaws.com"},
    "Action": "sts:AssumeRole"
  }
}
EOF

  tags = {
      tag-key = "EC2-to-CLoudwatch-logs"
  }
}

resource "aws_iam_role_policy" "metrics_policy" {
  name = "test_policy"
  role = aws_iam_role.log_sender_role.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "CloudWatchPutMetricData",
            "Effect": "Allow",
            "Action": "cloudwatch:PutMetricData",
            "Resource": "*"
        }
    ]
}
EOF
}

# resource "aws_iam_role_policy" "logs_policy" {
#   name = "test_policy"
#   role = aws_iam_role.log_sender_role.id

#   policy = <<EOF
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Sid": "CloudWatchPutLogData",
#             "Effect": "Allow",
#             "Action": "logs:PutLogEvents,
#             "Resource": "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:workshop-logging:log-stream:workshop-logging-stream"
#         }
#     ]
#   }
#   EOF
# }


resource "aws_cloudwatch_log_group" "log_group" {
  name = "workshop-logging"

  tags = {
    Name  = "Workshop log group"

  }
}

resource "aws_cloudwatch_log_stream" "log_stream" {
  name = "workshop-logging-stream"
  log_group_name = aws_cloudwatch_log_group.log_group.name
}


resource "aws_iam_instance_profile" "log_sender_profile" {
  name = "log_sender_profile"
  role = aws_iam_role.log_sender_role.name
}

# resource "aws_iam_role" "log_sender_role" {
#   name = "log_sender_role"
#   path = "/"

#   assume_role_policy = <<EOF
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Action": "sts:AssumeRole",
#             "Principal": {
#                "Service": "ec2.amazonaws.com"
#             },
#             "Effect": "Allow",
#             "Sid": ""
#         }
#     ]
# }
# EOF
# }



## Replace with ELB or assign only one IP
resource "aws_instance" "monitored_instance" {
  ami                = "ami-00068cd7555f543d5"
  iam_instance_profile = aws_iam_instance_profile.log_sender_profile.name
  instance_type      = var.instance_type
  key_name           = var.public_key_name
  subnet_id          = var.vpc_publicsubnet_id
  security_groups    = [aws_security_group.log_sender_sg.id]
  associate_public_ip_address = true 
  user_data = <<EOF
#! /bin/bash
cd
sudo apt-get update
sudo amazon-linux-extras install nginx1.12 -y
sudo service nginx start
sudo https://s3.amazonaws.com/amazoncloudwatch-agent/amazon_linux/amd64/latest/amazon-cloudwatch-agent.rpm
sudo rpm -U ./amazon-cloudwatch-agent.rpm
wget https://config-for-workshop.s3.amazonaws.com/amazon-cloudwatch-agent.json
sudo cp amazon-cloudwatch-agent.json /opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json
sudo systemctl enable amazon-cloudwatch-agent.service
sudo service amazon-cloudwatch-agent start
sudo yum install -y awslogs
sudo systemctl start awslogsd
sudo systemctl enable awslogsd.service
  EOF
  tags = {
    Name = "Monitored_instance"
  }
  lifecycle {
    ignore_changes = [security_groups]
    }  
  
}


# sudo apt install nginx -y
# sudo openssl req -x509 -nodes -days 365 -newkey "rsa:2048" -keyout /etc/nginx/cert.key -out /etc/nginx/cert.crt -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com"
# curl ${var.nginx_config_url} --output /home/ubuntu/nginx.conf
# sudo mv /home/ubuntu/nginx.conf /etc/nginx/conf.d/default.conf
# sudo sed -i 's/ES_endpoint/${var.es_endpoint}/g' /etc/nginx/conf.d/default.conf
# sudo sed -i 's/cognito_host/${var.cognito_userpoolhost}.auth.${var.region}.amazoncognito.com/g' /etc/nginx/conf.d/default.conf
# sudo rm /etc/nginx/sites-enabled/default
# sudo service nginx restart