output "monitored_instance" {
  description = "pub dns name for monitored instance"
  value =  aws_instance.monitored_instance.public_dns
}