# variable "ami_id" {}
variable "vpc_id" {}
variable "name" {}
variable "public_key_name" {}
variable "vpc_publicsubnet_id" {}
# variable "es_endpoint" {}
# variable "cognito_userpoolhost" {}
# variable "nginx_config_url" {}
variable "instance_type" {}
variable "region" {}

