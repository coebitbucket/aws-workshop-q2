# Prerequisites

You need to have:
- terraform v0.12.x installed (tested with v0.12.19).  
- SSH private/public keys (public is provided to Jenkins module, so you can have SSH access).  
- AWS credentials to any account with enough permissions to create EC2/VPC/IAM/S3/ElasticSearch resources in your console.  

## Manual Setup

Environment for this lab should be launced by corresponding Jenkins job. If you want to launch this lab manually, clone this repository:  
```$ git clone https://bitbucket.org/coebitbucket/aws-workshop-q2```
Enter `lab3-es-logging` folder and run:  
```$ terraform init``` to initialize providers and plugins  
```$ terraform plan``` to perform dry run and check if there is any errors  
```$ terraform apply``` to provision lab environment  

All needed information how to access your resources should be provided in outputs.  
If you scrolled down and lost your outputs, call them again with  
```$ terraform output```


## This lab includes following components:
1. Empty Cognito User Pool and Identity Poll - User should be created manually to access Kibana.
2. VPC based Elasticsearch cluster, which uses Cognito for authentication.
3. EC2 based proxy powered with nginx for routing external traffic to Elasticsearch.

## Instruction to this lab can be found in corresponding document.

May be changed optionally:

*  ```ubuntu_ami```	- ID of the ubuntu-based AMI for proxy server
*  ```proxy_instance_type```	- instance type for proxy (default t2.micro)
*  ```es_instance_type```		- instance type for ES (default t2.small.elasticsearch)
*  ```nginx_config```		- URL to download template config for proxy

Open terminal and type ```terraform apply```

Then type ```yes``` and press “enter”

Usually, it takes up to 20 minutes to deploy ES cluster

Some variables are optional to change. Everything is set up to launch anyway by default:
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| region | region in which you want to deploy stack | string | `us-east-1` | no |
| vpc | id of the VPC in which you deploy es and proxy | string | will be created automatically | no |
| subnet | ID of the subnet in which you deploy es and prox | string | will be created automatically | no |
| keypair| The key to access instance via SSH | string | will be created automatically | yes |
| ubuntu\_ami | ID of the ubuntu-based AMI for proxy server | string | `ami-04b9e92b5572fa0d1` | no |
| proxy\_instance\_type | Instance type for proxy | string | `t2.micro"` | no |
| nginx\_config | Link to download nginx config with proxy-forwarding | string | `https://s3publicbucketstuff.s3.amazonaws.com/nginx.conf` | no |

