variable "region" {
  type        = string
  default     = "us-east-1"
}

variable "nginx_config" {
  type        = string
  default     = "https://s3publicbucketstuff.s3.amazonaws.com/nginx.conf"
  description = "EC2 reverse-proxy nginx config location"
}
variable "vpc_id" {
  type        = string
  default     = ""
  description = "VPC_ID where the setup will be deployed or leave empty to deloy new"
}

variable "subnet_id" {
  type        = string
  default     = ""
  description = "public subnet ID where the setup will be deployed or leave empty to deloy new"
}

variable "ubuntu_ami" {
  type        = string
  default     = "ami-04b9e92b5572fa0d1"
  description = "ubuntu ami id for reverse nginx proxy curl apt and sed required"
}

variable "proxy_instance_type" {
  type        = string
  default     = "t2.micro"
}

variable "es_instance_type" {
  type        = string
  default     = "t2.small.elasticsearch"
}

variable "es_version" {
  type        = string
  default     = "7.1"
}

#variable "terraform_state_region" {
#  default     = "us-east-1"
#}

#variable "terraform_state_s3_bucket" {
#  default     = "stub"
#}

#variable "terraform_state_key" {
#  default     = "stub"
#}


