resource "aws_vpc" "main" {
  count = var.create_vpc
  cidr_block = "10.0.0.0/24"
  enable_dns_hostnames = true
} 

resource "aws_subnet" "public" {
  count = var.create_vpc  
  vpc_id     = aws_vpc.main[0].id
  cidr_block = "10.0.0.0/24"
  map_public_ip_on_launch = true
}

resource "aws_internet_gateway" "default" {
  count = var.create_vpc  
  vpc_id = aws_vpc.main[0].id
}

resource "aws_route_table" "public" {
  count = var.create_vpc  
  vpc_id = aws_vpc.main[0].id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.default[0].id
  }
}
resource "aws_route_table_association" "public" {
  count = var.create_vpc  
  subnet_id = aws_subnet.public[0].id
  route_table_id = aws_route_table.public[0].id
}