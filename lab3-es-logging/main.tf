provider "aws" {
	region     = var.region
}

data "aws_caller_identity" "current" {}

resource "random_string" "random" {
  length = 8
  special = false
  upper = false
}


#commented lines designed for remote state fetching

#data "terraform_remote_state" "vpc_and_subnet" {
#  backend = "s3"
#  config = {
#    region = var.terraform_state_region
#    bucket = var.terraform_state_s3_bucket
#    key    = var.terraform_state_key
#  }
#}

locals {
#  vpc_id = var.vpc_id == "" ? data.terraform_remote_state.vpc_and_subnet.outputs.vpc_id : ""
#  subnet_id = var.subnet_id == "" ? data.terraform_remote_state.vpc_and_subnet.outputs.subnet_id : ""
  unique_string = random_string.random.result
  
  vpc_id = var.vpc_id
  subnet_id = var.subnet_id
  create_vpc = var.vpc_id == "" || var.subnet_id == "" ? 1 : 0
}


module "vpc" {
  source = "./module_vpc"
  create_vpc = local.create_vpc
}

module "es_cognito" {
  source = "./module_cognito_es"
  
#required dynamic input:
  es_version                  = var.es_version
  es_instance_type            = var.es_instance_type
  region                      = var.region
  vpc_id                      = local.vpc_id == "" ? module.vpc.vpc_id : local.vpc_id
  vpc_publicsubnet_id         = local.subnet_id == "" ? module.vpc.subnet_id : local.vpc_id
  unique_string               = local.unique_string


  iam_role_es_log_writer      = data.aws_caller_identity.current.arn #service role for application to write logs to elastic
}

module "keypair" {
  source                      = "./module_keypair"
  region                      = var.region
  keypair_name                = "keypair-${local.unique_string}"
}


module "ec2_reverse_proxy" {
  source = "./module_ec2_reverse_proxy"
  name = "nginx proxy"

#required dynamic input:
  ami_id                      = var.ubuntu_ami
  instance_type               = var.proxy_instance_type
  region                      = var.region
  vpc_id                      = var.vpc_id == "" ? module.vpc.vpc_id : var.vpc_id
  vpc_publicsubnet_id         = var.subnet_id == "" ? module.vpc.subnet_id : var.subnet_id
  public_key_name             = module.keypair.public_key_name
  nginx_config_url            = var.nginx_config #downloadable URL for sample config file from the nginx directory in the module

  es_endpoint                 = trim("${module.es_cognito.es_endpoint}","https://")
  cognito_userpoolhost        = module.es_cognito.userpool_domain
}

module "ec2_log_sender" {
  source = "./module_ec2_log_sender"
  name = "nginx proxy"

#required dynamic input:
  # ami_id                     = var.ubuntu_ami
  instance_type               = var.proxy_instance_type
  region                      = var.region
  vpc_id                      = var.vpc_id == "" ? module.vpc.vpc_id : var.vpc_id
  vpc_publicsubnet_id         = var.subnet_id == "" ? module.vpc.subnet_id : var.subnet_id
  public_key_name             = module.keypair.public_key_name
  # nginx_config_url            = var.nginx_config #downloadable URL for sample config file from the nginx directory in the module

  # es_endpoint                 = trim("${module.es_cognito.es_endpoint}","https://")
  # cognito_userpoolhost        = module.es_cognito.userpool_domain
}

