output "proxy_dns" {
  description = "pub dns name for proxy"
  value =  aws_instance.nginx.public_dns
}