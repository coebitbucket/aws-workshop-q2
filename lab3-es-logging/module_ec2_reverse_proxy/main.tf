#THIS MODULE DEPLOYS NGINX-BASED REVERSE PROXY FOR ELASTICSEARCH CLUSTER
#VARS REQUIRED:
# - PUBLIC SUBNET
# - SECURITY GROUPS ID LIST
# - ELASTICSEARCH ENDPOINT
# - COGNITO USERPOOL HOSTNAME
# - SAMPLE NGINX CONFIG URL SUCH AS THOSE AS IN THE NGINX FOLDER OR HERE https://s3publicbucketstuff.s3.amazonaws.com/nginx.conf
resource "aws_security_group" "reverse_proxy_sg" {
  name        = "proxy-sg-${var.cognito_userpoolhost}"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

## Replace with ELB or assign only one IP
resource "aws_instance" "nginx" {
  ami                = var.ami_id ## Default is Ubuntu 18.04
  instance_type      = var.instance_type
  key_name           = var.public_key_name
  subnet_id          = var.vpc_publicsubnet_id
  security_groups    = [aws_security_group.reverse_proxy_sg.id]
  # vpc_security_group_ids = 
  associate_public_ip_address = true ## make variable
  user_data = <<EOF
#! /bin/bash
cd
sudo apt-get update
sudo apt install nginx -y
sudo openssl req -x509 -nodes -days 365 -newkey "rsa:2048" -keyout /etc/nginx/cert.key -out /etc/nginx/cert.crt -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com"
curl ${var.nginx_config_url} --output /home/ubuntu/nginx.conf
sudo mv /home/ubuntu/nginx.conf /etc/nginx/conf.d/default.conf
sudo sed -i 's/ES_endpoint/${var.es_endpoint}/g' /etc/nginx/conf.d/default.conf
sudo sed -i 's/cognito_host/${var.cognito_userpoolhost}.auth.${var.region}.amazoncognito.com/g' /etc/nginx/conf.d/default.conf
sudo rm /etc/nginx/sites-enabled/default
sudo service nginx restart
  EOF
  tags = {
    Name = "Nginx proxy"
  }
  lifecycle {
    ignore_changes = [security_groups]
    }  
  
}
