variable "aws_region" {
  description = "The AWS region in which Service will be created."
}

variable "aws_tests_region" {
  description = "The AWS region for running tests"
}

variable "vpc_name" {
  description = "The name of the VPC in which all the resources should be deployed"
  type        = string
  default     = "jenkins_vpc"
}

variable "instance_type" {
  description = "The type of instance to run"
  type        = string
  default     = "t3.medium"
}

variable "public_key_for_ec2_access" {
  description = "The key to access instance via SSH"
  type        = string
}

variable "admin_password" {
  description = "The password for Jenkins admin user"
  type        = string
  default     = "softserve"
}

variable "stack_url" {
  description = "The URL for TF stack code to deploy with Jenkins"
  type        = string
  default     = "git@bitbucket.org:coebitbucket/aws-workshop-q2.git"
}

variable "bitbucket_key_path" {
  description = "The path to a Private key for accessing Bitbucket"
  type        = string
}
