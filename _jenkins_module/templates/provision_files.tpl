runcmd:
  - [ wget, 'https://dl.google.com/go/go1.13.linux-amd64.tar.gz' ]
  - [ sudo, tar, '-C', /usr/local, '-xzf', 'go1.13.linux-amd64.tar.gz' ]
  - [ sudo, amazon-linux-extras, install, java-openjdk11 ]
  - [ sudo, wget, -O, '/etc/yum.repos.d/jenkins.repo', 'http://pkg.jenkins-ci.org/redhat/jenkins.repo' ]
  - [ sudo, rpm, --import, 'https://jenkins-ci.org/redhat/jenkins-ci.org.key' ]
  - [ wget, 'https://releases.hashicorp.com/terraform/0.12.18/terraform_0.12.18_linux_amd64.zip' ]
  - [ unzip, 'terraform_0.12.18_linux_amd64.zip' ]
  - [ chmod, 755, terraform ]
  - [ sudo, mv, terraform, '/usr/bin/' ]
  - [ sudo, yum, install, -y, jenkins-2.211-1.1.noarch, git, gcc ]
  - [ sed,  -i, s#JENKINS_ARGS=\"\"#JENKINS_ARGS=\"-Djenkins.install.runSetupWizard=false\"#, /etc/sysconfig/jenkins ]
  - [ wget, 'http://s3.${region}.amazonaws.com/${region}-sserve-jenkins-demo-${random}/_zipped_plugins.zip' ]
  - [ aws, s3, cp, 's3://${region}-sserve-jenkins-demo-${random}/bitb-ssh-key.priv', /var/lib/jenkins/bitb-ssh-key.priv ]
  - [ sudo, unzip, '-o', _zipped_plugins.zip, '-d', /var/lib/jenkins/plugins/ ]
  - [ sudo, chown, '-R', 'jenkins:jenkins', /var/lib/jenkins/plugins/ ]
  - [ sudo, service, jenkins, start ]

write_files:
  - path: /var/lib/jenkins/init.groovy.d/00-base.groovy
    permissions: "644"
    content: |
      import jenkins.model.*
      import jenkins.install.*
      import hudson.security.*
      import java.util.concurrent.*
      import hudson.slaves.EnvironmentVariablesNodeProperty
      import com.cloudbees.plugins.credentials.*
      import com.cloudbees.plugins.credentials.domains.*
      import com.cloudbees.jenkins.plugins.sshcredentials.impl.*

      def hudsonRealm = new HudsonPrivateSecurityRealm(false)
      hudsonRealm.createAccount("admin", "${admin_password}")

      def instance = Jenkins.getInstance()

      globalNodeProperties = instance.getGlobalNodeProperties()
      envVarsNodePropertyList = globalNodeProperties.getAll(EnvironmentVariablesNodeProperty.class)

      newEnvVarsNodeProperty = null
      envVars = null

      if ( envVarsNodePropertyList == null || envVarsNodePropertyList.size() == 0 ) {
        newEnvVarsNodeProperty = new EnvironmentVariablesNodeProperty()
        globalNodeProperties.add(newEnvVarsNodeProperty)
        envVars = newEnvVarsNodeProperty.getEnvVars()
      } else {
        envVars = envVarsNodePropertyList.get(0).getEnvVars()
      }

      envVars.put("STATE_BUCKET", "${state_bucket}")
      envVars.put("STATE_BUCKET_REGION", "${region}")
      envVars.put("TESTS_REGION", "${test_region}")

      instance.setNumExecutors(10)

      credentialsStore = instance.getExtensionList('com.cloudbees.plugins.credentials.SystemCredentialsProvider')[0].getStore()
      sshKey = new BasicSSHUserPrivateKey.DirectEntryPrivateKeySource(new File('/var/lib/jenkins/bitb-ssh-key.priv').getText('UTF-8'))
      sshCredentials = new BasicSSHUserPrivateKey(CredentialsScope.GLOBAL, 'bitbucketCredentials', 'jenkins', sshKey, null, 'Bitbucket key for Jenkins user')            
      credentialsStore.addCredentials(Domain.global(), sshCredentials)
  
      instance.save()

      //instance.getPluginManager().doCheckUpdatesServer()
      //plugins_to_install = [ 'workflow-aggregator', 'git', 'ansiColor' ]
      //plugins_to_install.each { 
      //  instance.updateCenter.getPlugin(it).deploy().get(3, TimeUnit.MINUTES)
      //}

  - path: /var/lib/jenkins/init.groovy.d/22-jobs.groovy
    permissions: "644"
    content: |
      import hudson.model.*
      import hudson.plugins.git.*
      import jenkins.branch.*
      import jenkins.model.*
      import jenkins.install.*
      import jenkins.plugins.git.GitSCMSource
      import jenkins.plugins.git.traits.*
      import org.jenkinsci.plugins.workflow.multibranch.*
      import com.cloudbees.hudson.plugins.folder.computed.PeriodicFolderTrigger
      import hudson.plugins.git.extensions.impl.CloneOption
      import jenkins.scm.impl.trait.WildcardSCMHeadFilterTrait

      def instance = Jenkins.getInstance()
      scmSource =  new GitSCMSource("${stack_url}")
      //cloneOption = new CloneOption(true, false, null, 10)
      //cloneOption.setDepth(1)
      //scmSource.setTraits([new BranchDiscoveryTrait(), new CloneOptionTrait(cloneOption)])
      scmSource.setCredentialsId('bitbucketCredentials')
      scmSource.setTraits([new BranchDiscoveryTrait(), new WildcardSCMHeadFilterTrait('master develop', null)])
      newBranchSource = new BranchSource(scmSource, new DefaultBranchPropertyStrategy(null))

      if (!instance.getJob('lab1-terraform-jenkins')) {
        job1            = instance.createProject(WorkflowMultiBranchProject, 'lab1-terraform-jenkins')
        job1.setSourcesList([newBranchSource])
        job1.getProjectFactory().setScriptPath('lab1-terraform-jenkins/Jenkinsfile')
        job1.addTrigger(new PeriodicFolderTrigger("1m"))
        job1.save()
      }

      if (!instance.getJob('lab2-api-gateway-cognito')) {
        job2            = instance.createProject(WorkflowMultiBranchProject, 'lab2-api-gateway-cognito')
        job2.setSourcesList([newBranchSource])
        job2.getProjectFactory().setScriptPath('lab2-api-gateway-cognito/Jenkinsfile')
        job2.addTrigger(new PeriodicFolderTrigger("1m"))
        job2.save()
      }

      if (!instance.getJob('lab3-es-logging')) {
        job3            = instance.createProject(WorkflowMultiBranchProject, 'lab3-es-logging')        
        job3.setSourcesList([newBranchSource])
        job3.getProjectFactory().setScriptPath('lab3-es-logging/Jenkinsfile')
        job3.addTrigger(new PeriodicFolderTrigger("1m"))
        job3.save()
      }
 
      if (!instance.installState.isSetupComplete()) {
        instance.setInstallState(InstallState.INITIAL_SETUP_COMPLETED)
        instance.restart()
      }

  - path: /var/lib/jenkins/init.groovy.d/99-reboot-if-not-setup.groovy
    permissions: "644"
    content: |
      import jenkins.model.*

      def instance = Jenkins.getInstance()
      
      if (!instance.installState.isSetupComplete()) {
        instance.restart()
      }